import express,{Request,Response} from 'express';

import UserModel from '../models/users';
import { createUser,findAndUpdate,findUser,deleteUser } from '../service/users.service';
import { log } from 'console';



const addUser = async (req:Request,res:Response)=>{

console.log("req.body.name",req.body.name);

const user = await createUser ({name: req.body.name,dept:req.body.dept})

 
    res.json({
        message:"User added",
     
        myData:user
    })
}

const updateUser = async (req:Request,res:Response)=>{


    const user = await findAndUpdate ({_id:req.params.id},{name:req.body.name,dept:req.body.dept},{new:true});
    
     
        res.json({
            message:"User Updated",
         
            myData:user
        })
    }

    const fetchUser = async (req:Request,res:Response)=>{


        const user = await findUser({_id:req.params.id})
        
         
            res.json({
               
                message:"User Information",
                myData:user
            })
        }

        const deleteUsers = async (req:Request,res:Response)=>{


            const user = await deleteUser({_id:req.params.id})
            
             
                res.json({
                    message:"User Deleted",
                 
                    myData:user
                })
            }
export{
    addUser,
    updateUser,
    fetchUser,
    deleteUsers
}