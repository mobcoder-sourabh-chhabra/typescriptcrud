import express,{Request,Response} from 'express';

import { addUser,updateUser,fetchUser,deleteUsers } from '../controllers/users';

 const router = express.Router();

 router.post("/addUser",addUser)
 router.put("/updateUser/:id",updateUser)
 router.get("/findUser/:id",fetchUser)
 router.delete("/deleteUsers/:id",deleteUsers)




 router.get("/about",(req:Request,res:Response)=>{
    res.json({
        msg:"About Page"
    })
 })
 export {
    router
 }